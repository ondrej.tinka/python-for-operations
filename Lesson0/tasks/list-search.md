Please, continue working with the file `employees.py`. Now, write a code that will perform the following actions:

- Employes: ['Frank', 'Bob', 'Amy', 'John', 'Kate', 'Ann', 'Ann', 'Ann']
- print employee at index 2
- print employee at index 7
- print second to fifth member
- print the index, at which the string 'John' encounters itself in `employees`: `John is at index: `

Example of running the script:

```
Employees: ['Frank', 'Bob', 'Amy', 'John', 'Kate', 'Ann', 'Ann', 'Ann']
At index 2 we have: Amy
At index 7 we have:  Ann
At indices 2 to 5 we have:  ['Amy', 'John', 'Kate', 'Ann']
Every third member: ['Frank', 'John', 'Ann']
John is at index: 3
```

