In order user's input can be comparable with our word 'Thursday', it will be the best if we changed users input into lowercase with the method `.lower()`.

Subsequently we print the result of comparison operation among lowercase user input and our lowercase "thursday" (the result will be `True` or `False`)


```python
days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
day = input('What is the name of the fourth day in a week? ')
day = day.lower()
print(day == days[3].lower())
```

You can also make your code shorter. We can ask for user input and convert it into lower case on one line:

```python
day = input('What is the name of the fourth day in a week? ').lower()
```
