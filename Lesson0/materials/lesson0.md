### PRVNÍ LEKCE

#### Co je to Python?

Python je **interpretovaný** *multiparadigmatický* programovací jazyk. Pojďme si to rozebrat.

##### Interpretovaný
Interpretovaný jazyk je takový, jehož kód před použitím nepotřebuje celý zkompilovat. Namísto kompilace se používá interpret - program, který rozumí danému kódu a řádek po řádku ho překládá a spouští. Mezi další interpretované jazyky se řadí:
- Perl
- JavaScript
- R
- pavršel
- MATLAB
- a mnoho dalších

V rámci urychlení běhu Pythnovských programů dochází při prvním spuštění k předkompilaci do tvz bytecode - nějakému více machine friendly seznamu instrukcí. Interpreter je pak schopen rychleji vykonávat dané instrukce. 

Interpreter Pythonu se nachází (překvapivě) v /usr/bin. Je ale třeba si dát pozor, většina systémů pod *python* spouští zastaralý Python 2. Proto je dobré si vytvořit alias python -> python3, nebo rovnou používat python3. Pozor, některé systémy - například CentOS v základu Python 3 nemají a musí se proto doinstalovat. (EPEL, Anaconda, nebo ze zdroje).

##### Multiparadigmatický
V Pythonu se dá programovat několika různými způsoby - využívat různá programovací paradigmata. Nejčastěji se (pravděpodbně) používá objektové paradigma následované procedurální, nebo pak funkcionálním.

Objektové paradigma stojí na vytváření **objektů**. Každý objekt má svou **třídu**, která určuje, jak má danný objekt vypadat a jaké má mít vlastnosti. 

Procedurální paradigma je asi nejznámější. Stojí na skládání jednotlivých dílčích kroků. Např.:
1. Vyvoř číslo.
2. K číslu, které si vytrvořil přičti jedna.
3. Vypiš výsledek do terminálu.

Funkcionální paradigma pak stojí na používání funkcí. Funkcí v rámci funkcionálního paradigmatu se myslí celkem specifická věc - nemá vnitřní stav a řeší se zejména vstupy a výstupy. Více k dočtení například na wikipedii. 

#### Jak funguje Python? - Trocha teorie na začátek
**Tato část vysvětluje pár věcí, které se dějí na pozadí, není nutné tuto část projít pro pochopení zbytku.**

**Úplným začátečníkům doporučuji tuto část radši zatím přeskočit.**

Ačkoli v Pythonu můžeme používat jakékoli paradigma, tak celý Python je postavený na objektech. Všechno v Pythonu je nějaký objekt - všechno má nějakou třídu. Třída, mimo již zmíněného, určuje i to, jak se mají objekty mezi sebou sčítat, jak porovnávat, jak se mají sčítat/násobit s objekty nějaké konkrétní jiné třídy atp. Celá čísla jsou tedy objekty třídy **int**, desetiná pak třídy **float** a funkce `print` je instancí třídy **funkce**. Aby toho nebylo málo, tak si povíme, že všechno v Pythonu je ukazatel - nějakýž odkaz na místo v paměti. Pokud tedy pak "kopírujeme" stylem:
```python
a = 1
b = a
```
Pak `b` bude ukazovat na stejné místo v paměti jako `a`. Pokud bychom tedy nějak *přímo* upravili `b`, změníme tak i `a`. Na druhou stranu možností jak přímo ovlivnit paměť na kterou je odkazováno není tolik. Pokud bychom pak k `b` něco přičetli, tak `a` vůbec neovlivníme protože operace sčítání vytvoří nový objekt.

### Začínáme
Abychom mohli vůbec nějak pracovat - zpracovávat nějaká data - bude dobré si nejdřív říct, jak je uložit. K ukládání dat za běhu programu slouží tvz. proměnné. V Pythonu se proměnné vytváří dost jednodušše. Stačí zapsat jméno proměnné a pomocí operátoru přiřazení **=** jí přiřadit nějakou hodnotu. Například takto:
```python
cislo = 42
```
Proměnné nemají restrikce na to co mohou ukládat - nemusíme deklarovat jaký datový typ bude uchovávat, ani kolik místa to zabere. Proto na jednom řádku můžeme do proměnné uložit číslo a hned na dalším nějaký text.
```python
a = 64
a = 'Meow'
```
Pojemnovávání proměnných má svá pravidla. Nemůžeme použít jedno z rezervovaných slov (postupně se s nimi seznámíme), nesmí začínat číslem a diakritikou. Jediná povolená diakritika na začátku jména proměnné je podtržítko `_ `. Jméno proměnné musí tvořit jeden řetězec - nesmí obsahovat mezeru.

Komentář v Python kódu se pak tvoří `#` na začátku komentovaného řádku.

### Základní datové typy
Základní datové typy (základní třídy) můžeme prozatím rozdělit do dvou kategorií.
1. Čísla
2. Sekvence


#### Čísla
V Pythonu jsou pouze dva základní číselné typy. Celá čísla - integer - **int**. Desetiná čísla - **float**. Python nijak nerozlišuje přesnost čísel, jako je to například v jazyce **CÉÉÉ**. Jako desetiný oddělovač se používá tečka.
```python
cele_cislo = 1
desetine_cislo = 1.0
#Toto je taky float
inkognito_cislo = 2.0
```

Mezi číselné datové typy se někdy také řadí **bool** - typ který nmůže nabývat pouze dvou hodnot: `True` nebo `False`.

Mezi int a float můžeme libovolně provádět operace. Python je chytrý a vždycky si převede int na float, pokud provádím operaci mezi int a float.
Podporované operace mezi čísly:
- Sčítání: `a + b`  
- Odčítání: `a - b`
- Násobení: `a * b`
- Dělení: `a / b` - pozor, vždy vrací float!
- Celočíselné dělení: `a // b` -  dělení se zaokrouhlením dolu - dostávám int. 5//2 = 2
- Mocnění: `a ** b` - a "na" b (a^b)
- Modulo: `a % b` - zbytek po celočíselném dělení

Jak porovnávat čísla?
- Je rovno: `a == b`
- Ostrá nerovnost: `<` nebo `>`
- Neostrá nerovnost: `<=` nebo `>=`

#### Sekvence
Sekvence v sobě umí uchovávat libovolný počet seřazených  prvků. Mezi sekvence se řadí:
1. string
2. list
3. tuple
4. range

String je řetězec - sekvence libovolných znaků. 
```python
s1 = 'String s jednoduchymi uvozovkami.'
s2 = "String s dvojtymi uvozovkami."
s3 = '''String s trojtymi jednoduchymi uvozovkami, ktery
umoznuje rozhathnout string
na vice radku'''
```

List je kontejner na libovolný počet libovolnýcyh objektů. Lze ho zvětšovat / měnit.
```python
l1 = [1, 2, 3]
l2 = ['a', 2, 3, '3', 'dva']
l3 = [1, 2.01, 'a', ['', 'a', 'brekeke], [-1, -9]]
```

Tuple je to samé jako list, je to nejde měnit.
```python
t1 = (1, 2, 3)
t2 = ('a', 2, 3, '3', 'dva')
t3 = (1, 2.01, 'a', ['', 'a', 'brekeke], (-1, -9))
```

Range je celočíselný rozsah. Probereme později.

Operace mezi sekvencemi musí být mezi sekvencemi stejného typu.
- Skládání: `seq1 + seq2` - výsledkem je nová sekvence slepená z původních. Např `'ab' + 'cd'` dá `'abcd'`
- Opakování: `seq * int` - výsledkem je nová sekvence několikráte se opakující za sebou. Počet opakování udává daný integer. `'a' * 3` dá `'aaa'`.
- Indexování: `seq[int]` - vrátí prvek na zadaném indexu - index začíná od 0.
- Řezání: `seq[[start:end]` - vrátí část původní sekvence začínající na indexu *start* a končící indexem `end`. Prvek na indexu *end* se už ve výběru nenachází.
- Řezání na steroidech: `seq[start:end:step]` - jak řezání s tím, že *step* značí kolikátý každý prvek se má vybrat.

Jak porovnávat sekvence?
- Stejný obsah: `seq1 == seq2`
- Testování příslušnosti: `a in seq` - zjišťujeme jestli je `a` v sekvenci `seq`

### Funkce
Funkce je nástroj, který umožňuje programátorům **seskupit blok kódu**, který dohromody **slouží společnému účelu**, tedy vykoná zamýšlenou akci. To znamená, že účelem funkcí je vykonat specifický úkol (například převést všechna písmena ve slovu z malých na velká písmena).

V Pythonu je spousta funkcí již předdefinovaných - rovnou připravených k použití. Jejich seznam lze na jít na: `https://docs.python.org/3/library/functions.html`.

Zatím si jich představíme jenom pár.
- **len** - vrátí délku sekvence
- **type** - vrátí třídu daného objektu
- **print** - vypíše do terminálu, vrací None
- **input** - vrátí vstup od uživatele jako string, optional argument - string který se zobrazí při prompt

Aby došlo k provedení funkce musí se zavolat. Za funkci stačí napsat kulaté závorky a do nich vepsat případné argumenty. Pythonovské funkce berou několik typů argumntů. O nich si povíme, až se budeme učit, jak psát vlastní funkce. V praxi volání funkce může vypadat třeba takto:
```python
print('Vitej v programu pro mereni delky slov.')
slovo = input('Zadej nejake slovo: ')
delka_slova = len(slovo)
print('Delka tveho slova', slovo, 'je', delka_slova)
```

#### Změna datového typu
Mnohokrát nastane situace, že budeme potřebovat změni datový typ. Například pokud si od uživatele vyžádáme nějaké číslo, například rok narození, a potom s ním budeme chtít provádět nějaké matematické operace - například ze zadaného roku narození spočítat věk. Funkce **input** nám ale **vždy** vrátí string a se stringem nemůžeme operovat jako s číslem (int). Potřebujeme tedy ze stringu, o kterém předpokládáme, že jsou v něm jen číselné znaky, udělat iteger (popřípadě float). Tato operace není nikterak složitá, pomůže nám funkce **int**.
```python
rok_narozeni = input('Zadej rok narozeni: ')
soucasny_rok = 2019
rn_int = int(rok_narozeni)
vek = rn_int - soucasny_rok
print('Vek je', vek, 'let')
```
Za předpokladu, že uživatel zadá něco smysluplného, se na konci dozvíme jeho věk. Pokud by zadal něco co na integer převést nejde, například nějaká písmena, Python vyvolá vyjímku a program spadne.

Převod lze samozřejmě provádět i mezi jinými datovými typy. Funkce pro převod se vždy sestává ze jména danného datového typu na který chceme převádět.
- int -> na integer
- float -> na float
- str -> na string
- list -> na list
- tuple -> na tuple

K vyzkoušení:
- string `'abcd'` na list
- string `12.5` na integer
- string `33.093` na float
- float `4.9` na integer
- integer 3 na float 
