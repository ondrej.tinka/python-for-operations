
- create list employees - `['Ann','Bob','Fred','Ann']`
- print how many times name 'Ann' can be found in`employees`: `The number of occurences of Ann: `


Example of running the script:

```
Employees: ['Ann','Bob','Fred','Ann']
The number of occurences of Ann: 2
```
