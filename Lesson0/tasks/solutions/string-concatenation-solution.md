```python
name = 'Bob'
print("Storing 'Bob' in name ...")

surname = 'Marley'
print("Storing 'Marley' in surname ...")

full_name = name + " " + surname
print(full_name)
```
