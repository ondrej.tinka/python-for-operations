- if we looked into the section 12.10. Useful String Methods - Finding Strings [H], we would find a method called `find`
- we can use it to find the first occurence of a subtring
- in our case, the subtring is letter 't'
- the method `find` starts its search from the left side of the string. If we wanted to find the last occurence, we would have to use its sibling `rfind`, which starts its search from the oposite (right) side of the string
- both `find` and `rfind` return a number. Therefore if we want to concatenate it with another string, we have to convert it into a string (lines 4 and 5)

```python
word = 'quetzalcoatl'
first_t = word.find('t') 
last_t = word.rfind('t')
print('The first occurence of the letter "t" is at the index: ' + str(first_t))
print('The last occurence of the letter "t" is at index: ' + str(last_t))
```
