```python
employees = ['Frank', 'Bob', 'Amy', 'John', 'Kate', 'Ann', 'Ann', 'Ann']

print('Employees:', employees)
print('At index 2 we have:', employees[2])
print('At index 7 we have:', employees[7])
print('At indices 2 to 5 we have:', employees[2:5])
print('Every third member:', employees[::3])
print('John is at index:', employees.index('John'))
```
