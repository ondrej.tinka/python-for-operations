- create list employees - `['Frank', 'Amy', 'John', 'Kate']`
- insert name 'Bob' stored in into the `employees` list at the index 1
- print the content of the variable `employees` introduced by the string: `Added new names to employees: `

Example of running the script:


```
Employees: ['Frank', 'Amy', 'John', 'Kate']
Added new name to employees: ['Frank', 'Bob', Amy', 'John', 'Kate']
```
