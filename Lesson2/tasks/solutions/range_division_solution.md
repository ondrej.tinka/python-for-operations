Při řešení tohoto úkolu je dobré použít metodu *devide and conquer* - **rozděluj a panuj**.

Rozdělíme si úkol na dvě části:
1. Jak zjisti dělitelnost přes zadaný rozsah
2. Jak zjištěnou infomaci uložit

Podíváme se na první část. Zde je opět dobré si úkol rozkouskovat na co nejmenší částí. Vyvstávají následující otázky:
- Jak zjistím jestli je nějaké číslo dělitelné nějakým jiným?
- Jak zjistím dělitelnost jednotlivých čísel ze zadaného rozsahu nějakým konkrétním číslem?
- Jak předchozí případ rozšířím abych zjišťoval dělitelnost čísly z rozsahu 2 až 9?

Odpověd na první otázku tkví v operaci *modulo* `%`. Na další otázku dostáváme odpověď v podobě smyčky **for**. Poslední odpověď pak nezahrnuje nic jiného, než vnoření smyčky, kterou už máme, pod další *for* cyklus, kterým budeme procházet rozsah čísel 2 až 9.

V Pythonu to pak bude vypadat následovně:
```python
def range_division(start, end):
  divisors = range(2, 9)
  dividends = range(start, end)
  for divisor in divisors:
    for divident in dividends:
      if divident % divisor == 0:
        ...
```

Přicházíme k druhé části - jak uložit zjištěnou informaci. Vzhledem k tomu, že potřebujeme k nějaké danné hodnotě přiřadit *list* generovaných hodnot, bude nejlepší použít slovník.

Jedna z možností jak to celé vyřešit, je, připravit si *kostru*  do které se pak naplní informace.
```python
def range_division(start, end):
  divisors = range(2, 9)
  dividends = range(start, end)
  result = {2:[], 3:[], 4:[], 5:[], 6:[], 7:[], 8:[], 9:[]}

  for divisor in divisors:
    for dividend in dividends:
      if dividend % divisor == 0:
        result[divisor] = result[divisor] + [dividend]

  return result
```

Lepší ale bude generovat i klíče pro náš slovník. Nyní se situace trochu zesložiťuje. Musíme zachytit případ, kdy klíč ve slovníku ještě není - musíme vymyslet jak ho tam vhodně přidat. V tomto nám pomůže metoda `get`.

```python
def range_division(start, end):
  divisors = range(2, 9)
  dividends = range(start, end)
  result = {}

  for divisor in divisors:
    for dividend in dividends:
      if dividend % divisor == 0:
        result[divisor] = result.get(divisor, []) + [dividend]

  return result
```
