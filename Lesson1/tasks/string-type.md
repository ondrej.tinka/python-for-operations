Your task is to create a script called `string_type.py` that will:

- Ask user for any string
- Determine, whether the string entered 
    - contains only numbers - digits - in that case the program should print to the terminal: `'Numeric'`
    - contains only letters - in that case the program should print to the terminal: `'Letters Only'`
    - otherwise print to terminal: `'Mixed'`

Example of running the script:

```
Give me some input: Abc
Letters Only
```

```
Give me some input: 4every1
Mixed
```

```
Give me some input: 99
Numeric
```
