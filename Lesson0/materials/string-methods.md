We've created quite a list here, but it still doesn't include all the methods. For more info on string method go to <a href="https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str" target="_blank">Python documentation</a>. Each method is preceeded by a string - `'string'.method(argument)`


|Operation|Syntax|Description| Example | Output |
|:-------:|:----:|:---------:|:-------:|:------:|
|Letter change| `.lower()` | Return string with all letters lowercased | `'HA HA'.lower()`| `'ha ha'` |
|Letter change| `.upper()` | Return string with all letters uppercased | `'he he'.upper()` | `'HE HE'` | 
|Letter check| `.islower()` | Returns boolean value *True*, if all items in string are lowercased. Otherwise returns *False* | `'David'.islower()`| `False`| 
|Letter check| `.isupper()` | Returns boolean value *True*, if all items in string are uppercased. Otherwise returns *False*  | `'PETR'.isupper()`| `True`|
|Letter change| `.title()` | Returns a new string, where every word begins with capital letter. If a number sequence precedes letters, the method will uppercase the first letter anyway. Also if there is apostrophe within the word, the letter succeding the apostrophe is also capitalized. | `'123word'.title()`|`123Word`|
|Letter change| `.replace(what,forwhat)` | Replaces a substring for another substring inside the operated string.| `'Race'.replace('R','F')`| `'Face'`|
|Letter check| `.istitle()` | Returns boolean value *True*, if the first character is a capital letter. Otherwise returns *False*  | `'Martin'.istitle()`| `True`|
|Letter change| `.swapcase()` | Swaps the upper and lowercase letters. | `'Mr. Jones PhD'.swapcase()`| `'mR. jONES pHd.'`|
|Split by symbol| `.split('a')` | Splits the given string by the symbol given in the parentheses into a list. If the parentheses are empty, the method splits the list by space | `splitted = 'This is my car'.split()` | `['This', 'is', 'my', 'car']`|
|Split by delimeter| `.splitlines([keepends])` | Splits the string on line end delimiters. Flag **keepends** - tells whether line delimiters ('\n' or '\r' etc.) should be kept | `'This is the first line\nThis is the second line'.splitlines(keepends = True)` | `['This is the first line\n',This is the second line']`|
|Split by separator| `.partition(separator)` | Returns a tuple of part of the string preceding the separator, separator, part of the string succeeding the separator | `up-to-date'.partition('-')` | `('up', '-', 'to-date')`|
|Strip of symbols| `.strip()` | Strips the given string(s) of the symbols we want to remove. The symbols will be removed only if they are at the beginning and the end. | `'www.example.cz'.strip('www.cz')`| `'example'` |
|Character type| `.isdecimal()` | Returns **True** if all the characters inside the string are decimal characters ('0','1','2','3','4','5','6','7','8','9') and there is at least 1 character, otherwise **False**  | `'1234'.isdecimal()`| `True`|
|Character type| `.isdigit()` | Returns **True** if all the characters inside the string are decimal characters or subscripts or superscripts (e.g. "\u00B2") and there is at least one character, otherwise **False**.  | `'\u00B2'.isdigits()`| `True`|
|Character type| `.isnumeric()` | Returns **True** if all the characters inside the string are decimal characters, subscripts, superscripts (e.g. "\u00B2"), fractions (e.g. "\u00BC"), roman numerals (e.g. "\u2165"), currency numerators etc. (all characters that have the Unicode numeric value property), and there is at least one character, **False** otherwise.   | `'\u2165'.isnumeric()`| `True`|
|Character type| `.isalpha()` | Returns boolean value *True*, if all the characters are letters. Otherwise returns *False*  | `'asdvfb'.isaplha()`| `True`|
|Character type| `.isalnum()` | Return true if all characters in the string are alphanumeric and there is at least one character, false otherwise (Basically all the Character Type methods above - `.isdecimal()`,`.isdigit()`,`.isnumeric()`, `.isalpha()`.| `'Z312'.isalnum()`| `True`|
|Search| `.find('m')` | Finds a character in a string and returns its index  |`'Thomas'.find('m')`| `3`|
|Join by symbol| `.join(seq)` | Joins the items of the sequence in the parentheses by the symbol written in the string |`':'.join(['a', 'b'])`| `'a:b'` |

