### DRUHÁ LEKCE

V této lekci si probereme řídící struktury. 

#### IF
Často budeme potřebovat aby se provedl nějaký kód na základě nějaké podmínky. K tomuto účelu slouží rezervované slov **if**. Python za tímto slovem očekává nějaký výraz, ze kterého jde nějakým způsobem získat `True` nebo `False`. To může být například:

- Porovnávání - `1 < 2`
- Výstup funkce / metody - `'ahoj'.isupper()`
- Proměnná - Python na proměnou použije funkci `bool`

Za výrazem pak následuje dvojtečka a na dalším řádku / řádcích pak odsazený kód, který se provede, pokud výraz vrátí `True`. Velikost odsazení může být libovolná za předpokladu, že je v rámci celého skriptu stejná. Nejčastěji se používá jeden tabulátor, což je ekvivalentem čtyř mezer.

Příkald celého rozhodovacího procesu:
```python
a = int(input('Zadej prvni cislo: '))
b = int(input('Zadej druhe cislo: '))
if a < b:
    print('Prvni cislo je mensi nez druhe.')
```

Pokud výraz za slovem **if** vrátí `False`, odsazený kód se přeskočí a program normálně pokračuje dál. Pokud je v tomto případě potřeba prověřit ještě jiný případ, respektive několik jiných případů, Python nabízí slovo **elif**. Za tímto slovem opět následuje nějaký výraz. 
Rozšíření předchozího příkladu:
```python
a = int(input('Zadej prvni cislo: '))
b = int(input('Zadej druhe cislo: '))
if a < b:
    print('Prvni cislo je mensi nez druhe.')
elif b < a:
    print('Druhe cislo je mensi nez prvni.')
```

Výraz za slovem **elif** se vyhodnocuje pouze v případě, že výraz u předhozího **elif** nebo **if** vrátil `False`.

Pokud chceme pokrýt všechny ostatní případy - výraz u **if** a všech **elif** vrátil `False`, musíme použít slovo **else**.
```python
a = int(input('Zadej prvni cislo: '))
b = int(input('Zadej druhe cislo: '))
if a < b:
    print('Prvni cislo je mensi nez druhe.')
elif b < a:
    print('Druhe cislo je mensi nez prvni.')
else:
    print('Cisla jsou stejna')
```

#### Smyčky
Smyčky slouží k automatizaci určitých částí kódu. Místo abysme nějaký kus kódu psali 10x, ve smyčce ho napíšeme pouze jendou. Pokud smyčku vhodně napíšeme, můžeme i za běhu programu ovlivnit počet opakování, což by s 10x opsaným kódem nešlo. 

#### Smyčka while
Nejdířve se podíváme na smyčku **while**. Slovo while česky znamená zatímco, což je velmi vypovídající o tom, jak smyčka funguje. Kód ve smyčce se porvádí do té doby, dokud platí nějaká podmínka. Dalo  by se řící, že tato smyčka je jako nádstavba **if**. Syntax je tu stejná. Namísto slova **if** se akorát používá **while**. Odsazený kód se pak provádí do té doby, dokud výraz za slovem **while** vrací `True`.

Nyní si představíme tři různé typy smyček, abychom viděli jak různě jde s while pracovat.

- **Úkol:** Vypsat postupně všechny prvky listu.
- **Řešení:** Pomocná proměnná, kterou budeme při každém průchodu smyčkou načítat, nám poslouží jako index pro list.
```python
list1 = [1, 2, 3, 4, 5, 6]
index = 0
while index < len(list):
    print(list1[index])
    index += 1
```

- **Úkol:** Naplnit list vstupem od uživatele.
- **Řešení:** Budeme se ptát jestli délka listu již dosáhla nějakého definovaného čísla. Pokud ne, zeptáme se uživatele na vstup.
```python
pocet = int(input('Zadej pocet cisel: '))
cisla = []
while len(cisla) < pocet:
    cislo = int(input('Zadej cislo: '))
    cisla.append(cislo)
```

- **Úkol:** Vypsat postupně všchny prvky v listu. List nemusí být zachován.
- **Řešení:** Za slovo while napíšme pouze názecv listu. V rámci smyčky budeme využívat metodu `pop`, která vyjme prvek z listu. Jakmile se vyjme poslední prvek - list je prázdný - vrací `False`.
```python
list1 = [1, 2, 3, 4, 5]

while list1:
    print(list1.pop())
```

#### Smyčka for
Smyčka **for** funguje podobně jako v bashi. Slouží k procházení objektů, které v sobě něco uchovávají. For cyklem lze tedy procházet například sekvence, které už jsme potkali, ale také i mnohé další. Syntax je opět podobná bashi. První přichází klíčové slovo **for**, pak následuje jméno porměnné kam se bude ukládat jeden prvek z procházeného objektu, dále následuje klíčové slovo **in** a nakonec objekt, který procházíme.
```python
zviratka = ['Kocka', 'Pes', 'Krecek', 'Zizala']
for zviratko in zviratka:
    print(zviratko)
``` 

#### Řízení průběhu smyčky
Python, stejně jako i jiné jazyky, nabízí nástroje jak ovlivnit chod smyčky. Jsou jimi klíčová slova **break** a **continue**. Obě slova fungují jak se smyčkou **for**, tak se smyčkou **while**.

Slovo **break**, jak jeho význam naznačuje,slouží k přerušení smyčky. Pokud Python na toto slovo narazí, okamžitě ukončujě průběh smyčky, vyskakuje ven a pokračuje v normálním kódu po smyčce.

Následující kód by vypsal pouze čísla menší než 100:
```python
konec = int(input('Zadej konec rozsahu: '))
r = range(konec)

# break ve for cyklu
for i in r:
    if i > 100:
	break
    print(i)

# break ve while smycce
index = 0
while i<len(r):
    if i > 100:
	break
    print(r[index])
    index += 1
```

Slovo **continue** je téměř protikladem slova **break**. Pokud Python narazí na toto slovo, *pokračuje* na další krok smyčky. V případě for cyklu tedy vytáhne z procházeného objektu další prvek a začne znovu vykonávat kód. V případě smyčky while pouze skočí na začátek - podívá se na test.
Následující kód vypíše pouze sudá čísla:
```python
r = range(10)

# continue ve for cyklu
for i in r:
    if i % 2 == 1:
	continue
    print(i)

# continue s while smyckou
index = 0
while index<len(r):
    if r[index] % 2 == 1:
	index +=1
	continue
    print(r[index])
    index += 1
```

