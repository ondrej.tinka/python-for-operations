We can see from the task description, that there are two possible outcomes from the program:

1. print `'Welcome!'`
2. **otherwise** print `'The input does not match'`


That tells us that we have to set up a conditions made of two branches - `if - else`.

To check, whether a string starts with a given letter, we have to extract this letter first using indexing. The first letter encounters itself at index 0.

To check, whether the first letter is one of the letters 'a','e','f','q','z', we just need to use **membership testing** applied to a string of those letters `if password[0] in 'aefqz`.

```python
password = input('Please enter the password: ')
if password[0] in 'aefqz':
	print('Welcome!')
else:
	print('The input does not match')
```
