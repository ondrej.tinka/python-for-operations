Your task is to create a script called `convert_day.py` that will: 

- ask for a number between 1 to 7
- return the name of corresponding weekday (1 - `'Monday'`, 2-`'Tuesday'`, 3 - `'Wednesday'`, 4 - `'Thursday'`, 5 - `'Friday'`, 6 - `'Saturday'`, 7 - `'Sunday'`)
- if no input has been provided (user hitting enter without typing anything), the program should print: 'No input provided'
- if the input is not a number the program should print: 'Enter only numbers, please'


Example of running the script:

```
Please enter the number of the day: 3
Wednesday
```

```
Please enter the number of the day: abc
Enter only numbers, please
```

```
Please enter the number of the day: 
No input provided
```
