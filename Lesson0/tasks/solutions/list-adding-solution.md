- let's use the `concatenation` `+` between two lists - `candidates` and `['Bob', 'Ann']`
- concatenation extends the list on the right side of the expression
- the result should be stored back into a variable `candidates`

```python
candidates = []
candidates = candidates + ['Bob', 'Ann']
print("Added new names to candidates: ['Bob', 'Ann']")
```

### OR

```python
candidates = []
print("Candidates: ", candidates)

candidate1 = 'Bob'
candidate2 = 'Ann'

candidates.append(candidate1
candidates.append(candidate2)))

print("Added new names to candidates: ", candidates)
```
