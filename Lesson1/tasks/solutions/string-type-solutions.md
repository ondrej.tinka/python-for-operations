We ask for the user input using `input()` function. We store the input inside the variable `user_input`.

After that we check, whether the variable `user_input` refers to a string that:

1. Contains letters only - `if user_input.isalpha():`
2. Contains numbers only - `elif user_input.isnumeric():`

If none of the above is True, the code inside the `else:` clause is executed.

```python
user_input = input('Give me some input: ')

if user_input.isalpha():
	print('Letters Only')
elif user_input.isnumeric():
	print('Numeric')
else:
	print('Mixed')
```
