- Emails are represented as strings
- Strings are sequences
- To extract a part of the string, we can use slicing operation (`email[start:stop]`)
- To do that, we need to find out, where the slice should begin (start index) and where it should end (stop index)
- We actually already know, that the start index is 0 - we want to extract substring from from the beginning up to "@"
- So our task here is to determine, at which index the `"@"` symbol encounters itself
- There are multiple ways, we can accomplish this:

    1. use `email.find(substr)` method - this operation looks for a substring `subtstr` ("@") in the `email` and returns the index at which it was found
    
    2. user `email.split(substr)` method - this operation splits the `email` string on delimiter `substr`("@") and returns a list of parts, the original string was broken into


### Method .find() 

```python
email1 = 'mr.reilly@gmail.com'
email2 = 'john55@yahoo.com'
email3 = 'elgringo@atlas.sk'

stop1 = email1.find('@')
stop2 = email2.find('@')
stop3 = email3.find('@')

print(email1[:stop1])
print(email2[:stop2])
print(email3[:stop3])
```

### Method .split() 
First we split the email in 2 parts and then we extract the `index [0]`. You can use `print()` function to inspect what is actually happening in the first and the second step.

```python
email1 = 'mr.reilly@gmail.com'
email2 = 'john55@yahoo.com'
email3 = 'elgringo@atlas.sk'

email1_part = email1.split('@')[0]
email2_part = email2.split('@')[0]
email3_part = email3.split('@')[0]

print(email1_part)
print(email2_part)
print(email3_part)
```
