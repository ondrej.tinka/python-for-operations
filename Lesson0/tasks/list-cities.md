Create a file `cities.py`. Inside the file do following: 
1. Create a list containing following cities: New York, Los Angeles, Berlin, Prague, London
2. Print the list.
3. Print out the city at the index 2 introduced by the string: `At index 2 we have:`
4. Print out the city at the last index introduced by the string: `Last city, located at index <index_num>, we have:`

<br>
Example of running the script in terminal:

```
~/PythonBeginner/Lesson2$ python cities.py
Cities in my list: ['New York', 'Los Angeles', 'Berlin', 'Prague', 'London']
At index 2 we have: Berlin
Last city, located at index 4, we have: London
```

