Create a python script named `fullname.py` that will perform the following actions:

- Store the string value **'Bob'** inside the variable `name`. 
- Print the information that value **'Bob'** has been stored, for example: *"Storing 'Bob' in name ..."*
- Then  store the string **'Marley'**  a variable `surname`. 
- Print the information that value **'Marley'** has been stored, for example: *"Storing 'Marley' in surname ..."*
- Lastly create a variable `full_name`, assign it the result of concatenation of values in variables `name` and `surname` and print the content of `full_name` to the terminal.

Example of script usage in terminal:

```
~/PythonBeginner/Lesson1$ python fullname.py
Storing 'Bob' in name ...
Storing 'Marley' in surname ...
Bob Marley
```

