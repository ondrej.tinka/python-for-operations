Create a python script named `find_t.py` that will perform the following actions:

- print out at what index is the first occurence of the letter 't' in the word 'quetzalcoatl'
- print out at what index is the last occurence of the letter 't' in the word 'quetzalcoatl'

Example of running the script in terminal:
```
~/PythonBeginner/Lesson1 $ python find_t.py
The first occurence of the letter "t" is at the index: 3
The last occurence of the letter "t" is at index: 10
```
