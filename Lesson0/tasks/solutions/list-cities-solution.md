- first, we create a variable `cities`, where we store list[] containing names of cities
- if we want to print city at the index 2, we need to use `cities[2]` statement
- you know that index starts from 0, so last index can be determined as length of list - 1, so `len(cities) - 1`


```python
cities = ['New York', 'Los Angeles', 'Berlin', 'Prague', 'London']
print('Cities in my list: ' + str(cities))
print('At index 2 we have: ' + cities[2])
last_index = len(cities) - 1
print('Last city, located at index ' + str(last_index) + " we have: " cities[last_index])
```
